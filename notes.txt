https://github.com/dave/jennifer
https://www.carlomaiorano.me/golang/2019/10/03/generating-code-golang.html
https://www.youtube.com/watch?v=uBPXNREhZZw


package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	. "github.com/dave/jennifer/jen"
)

func main() {

	MF()
	f := NewFilePathName("./tests/a.go", "aa")
	f.Func().Id("a").Params().Block()
	f.Type().Id("foo").Struct(
		List(Id("x"), Id("y")).Int(),
		Id("u").Float32(),
	)

	
	buf := &bytes.Buffer{}
	err := f.Render(buf)
	if err != nil {
		fmt.Println(err.Error())
	} else {
		fmt.Println(buf.String())
	}

	if err := ioutil.WriteFile("./tests/a.go", buf.Bytes(), 0644); err != nil {
		panic(err)
	}

}

func MF() {
	_, err := os.Stat("tests")

	if os.IsNotExist(err) {
		errDir := os.MkdirAll("tests", 0755)
		if errDir != nil {
			log.Fatal(err)
		}

	}
}
