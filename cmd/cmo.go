/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	. "github.com/dave/jennifer/jen"
	"github.com/spf13/cobra"
)

// cmoCmd represents the cmo command
var cmoCmd = &cobra.Command{
	Use:   "cmo",
	Short: ``,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("cmo called")
		MF()
		f := NewFilePathName("./tests/a.go", "aa")
		f.Func().Id("a").Params().Block()
		f.Type().Id("foo").Struct(
			List(Id("x"), Id("y")).Int(),
			Id("u").Float32(),
		)

		buf := &bytes.Buffer{}
		err := f.Render(buf)
		if err != nil {
			fmt.Println(err.Error())
		} else {
			fmt.Println(buf.String())
		}

		if err := ioutil.WriteFile("./tests/a.go", buf.Bytes(), 0644); err != nil {
			panic(err)
		}
	},
}

func init() {
	rootCmd.AddCommand(cmoCmd)

}

func MF() {
	_, err := os.Stat("tests")

	if os.IsNotExist(err) {
		errDir := os.MkdirAll("tests", 0755)
		if errDir != nil {
			log.Fatal(err)
		}

	}
}
