module gitlab.com/zendrulat123/jentest

go 1.16

require (
	github.com/dave/jennifer v1.4.1
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/cobra v1.1.3
	github.com/spf13/viper v1.7.0
)
